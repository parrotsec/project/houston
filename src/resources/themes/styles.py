"""Module for managing themes for the application's GUI."""

# Consts Colors Generic
from src.resources.themes.color_consts import WHITE, BLACK
# Consts Colors Default Theme
from src.resources.themes.color_consts import (
    BG_WIDGET_DEFAULT,
    BG_ITEM_DEFAULT,
    BORDER_ITEM_DEFAULT,
    BG_BUTTON_DEFAULT,
    BORDER_BUTTON_DEFAULT,
    BG_ITEM_SELECTED_DEFAULT
)
# Consts Colors Hack The Box Theme
from src.resources.themes.color_consts import (
    BG_WIDGET_HACKTHEBOX,
    BG_ITEM_HACKTHEBOX,
    BORDER_ITEM_HACKTHEBOX,
    BG_BUTTON_HACKTHEBOX,
    BORDER_BUTTON_HACKTHEBOX,
    BG_ITEM_SELECTED_HACKTHEBOX
)
# Consts Colors Greenlight Theme
from src.resources.themes.color_consts import (
    BG_WIDGET_GREENLIGHT,
    BG_ITEM_GREENLIGHT,
    BORDER_ITEM_GREENLIGHT,
    BG_BUTTON_GREENLIGHT,
    BORDER_BUTTON_GREENLIGHT,
    BG_ITEM_SELECTED_GREENLIGHT
)
# Consts Colors Sunset Theme
from src.resources.themes.color_consts import (
    BG_WIDGET_SUNSET, BG_ITEM_SUNSET,
    BORDER_ITEM_SUNSET,
    BG_BUTTON_SUNSET,
    BORDER_BUTTON_SUNSET,
    BG_ITEM_SELECTED_SUNSET
)

themes = {
    'Default': f"""
        QWidget {{
            background-color: {BG_WIDGET_DEFAULT};
            color: {WHITE};
        }}
        QLineEdit, QTextEdit {{
            background-color: {BG_ITEM_DEFAULT};
            color: {WHITE};
            border: 1px solid {BORDER_ITEM_DEFAULT};
            border-radius: 10px;
            padding: 5px;
        }}
        QPushButton {{
            background-color: {BG_BUTTON_DEFAULT};
            color: {BLACK};
            border: 1px solid {BORDER_BUTTON_DEFAULT};
            padding: 5px;
            border-radius: 10px;
        }}
        QInputDialog {{
            padding: 5px;
            border-radius: 10px;
        }}
        QListWidget, QTreeWidget {{
            background-color: {BG_ITEM_DEFAULT};
            color: {WHITE};
            border-radius: 5px;
        }}
        QListWidget::item:selected, QTreeWidget::item:selected {{
            background-color: {BG_ITEM_SELECTED_DEFAULT};
            color: {BLACK};
        }}
    """,
    'Hack The Box': f"""
        QWidget {{
            background-color: {BG_WIDGET_HACKTHEBOX};
            color: {WHITE};
        }}
        QLineEdit, QTextEdit {{
            background-color: {BG_ITEM_HACKTHEBOX};
            color: {WHITE};
            border: 1px solid {BORDER_ITEM_HACKTHEBOX};
            border-radius: 10px;
            padding: 5px;
        }}
        QPushButton {{
            background-color: {BG_BUTTON_HACKTHEBOX};
            color: {BORDER_ITEM_HACKTHEBOX};
            border: 1px solid {BORDER_BUTTON_HACKTHEBOX};
            padding: 5px;
            border-radius: 10px;
        }}
        QInputDialog {{
            padding: 5px;
            border-radius: 10px;
        }}
        QListWidget, QTreeWidget {{
            background-color: {BG_ITEM_HACKTHEBOX};
            color: {WHITE};
            border-radius: 5px;
        }}
        QListWidget::item:selected, QTreeWidget::item:selected {{
            background-color: {BG_ITEM_SELECTED_HACKTHEBOX};
            color: {BLACK};
        }}
    """,
    'Greenlight': f"""
        QWidget {{
            background-color: {BG_WIDGET_GREENLIGHT};
            color: {BLACK};
        }}
        QLineEdit, QTextEdit {{
            background-color: {BG_ITEM_GREENLIGHT};
            color: {BLACK};
            border: 1px solid {BORDER_ITEM_GREENLIGHT};
            border-radius: 10px;
            padding: 5px;
        }}
        QPushButton {{
            background-color: {BG_BUTTON_GREENLIGHT};
            color: {WHITE};
            border: 1px solid {BORDER_BUTTON_GREENLIGHT};
            padding: 5px;
            border-radius: 10px;
        }}
        QInputDialog {{
            padding: 5px;
            border-radius: 10px;
        }}
        QListWidget, QTreeWidget {{
            background-color: {BG_ITEM_GREENLIGHT};
            color: {BLACK};
            border-radius: 5px;
        }}
        QListWidget::item:selected, QTreeWidget::item:selected {{
            background-color: {BG_ITEM_SELECTED_GREENLIGHT};
            color: {BLACK};
        }}
    """,
    'Sunset': f"""
        QWidget {{
            background-color: {BG_WIDGET_SUNSET};
            color: {WHITE};
        }}
        QLineEdit, QTextEdit {{
            background-color: {BG_ITEM_SUNSET};
            color: {WHITE};
            border: 1px solid {BORDER_ITEM_SUNSET};
            border-radius: 10px;
            padding: 5px;
        }}
        QPushButton {{
            background-color: {BG_BUTTON_SUNSET};
            color: {WHITE};
            border: 1px solid {BORDER_BUTTON_SUNSET};
            padding: 5px;
            border-radius: 10px;
        }}
        QInputDialog {{
            padding: 5px;
            border-radius: 10px;
        }}
        QListWidget, QTreeWidget {{
            background-color: {BG_ITEM_SUNSET};
            color: {WHITE};
            border-radius: 5px;
        }}
        QListWidget::item:selected, QTreeWidget::item:selected {{
            background-color: {BG_ITEM_SELECTED_SUNSET};
            color: {BLACK};
        }}
    """
}
