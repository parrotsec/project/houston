from PySide6.QtWidgets import QWidget, QVBoxLayout, QLineEdit
from PySide6.QtCore import QTimer
from src.common.tool_card.helpers.layout_components import (
    build_layout,
    usage_examples_section,
    container_buttons_section,
)
from src.controller.docker import DockerController
from src.common.tool_card.helpers.timer_setup import setup_timer


class ToolCard(QWidget):
    """
    This widget displays detailed information about a specific tool,
    Displays a tool's details, usage examples, and Docker controls.
    Periodically updates the tool's image download status.
    """
    def __init__(self, tool, tool_item):
        """
        Initializes UI components and sets up the image update timer.
        """
        super().__init__()
        self.tool = tool
        self.tool_item = tool_item
        self.flags_input = None
        self.run_button = None
        self.stop_button = None
        self.download_status_label = None
        self.timer = QTimer()
        self.update_download_status = self.update_download_status
        self.stop_container = self.stop_container

        layout = QVBoxLayout()
        details_layout = build_layout(self)
        layout.addLayout(details_layout)
        usage_examples_section(self, layout)
        self.input_flags(layout)
        container_buttons_section(self, layout)
        self.setLayout(layout)
        self.timer = setup_timer(self)

    def update_download_status(self):
        """
        Update the image download status of the tool.
        """
        DockerController.check_container_status(self.tool, self.tool_item, self.download_status_label, self.run_button)

    def stop_container(self):
        """
        Stop the Docker container for the tool.
        """
        DockerController.stop_container(self.tool, self)

    @staticmethod
    def get_placeholder_text(tool_name: str) -> str:
        """
        Set the placeholder text for the flags input field based on the tool name.
        """
        placeholders = {
            "nmap": "Enter flags (e.g., -p 80 192.168.0.1)",
            "bettercap": "Enter flags (e.g., --proxy)",
            "sqlmap": "Enter flags (e.g., -u 'http://example.com')",
            "netexec": "Enter flags (e.g., --help)",
            "evil-winrm": "Enter flags (e.g., --help)"
        }
        return placeholders.get(tool_name, "Enter flags for the tool")

    def input_flags(self, layout):
        """
        Create and add the flags input field to the provided layout.
        """
        if self.tool["name"].lower() in ["nmap", "bettercap", "sqlmap", "netexec", "evil-winrm"]:
            self.flags_input = QLineEdit(self)
            self.flags_input.setPlaceholderText(self.get_placeholder_text(self.tool["name"].lower()))
            layout.addWidget(self.flags_input)

    def get_flags(self) -> str:
        """
        Returns the flags that were entered by the user,
        or an empty string if there is no input field for the flags.
        """
        return self.flags_input.text() if self.flags_input else ""
