from PySide6.QtCore import QTimer

def setup_timer(tool_card):
    """
    Set up QTimer to periodically update the image download status of a docker tool.

    This function initializes a QTimer, connects its timeout signal to the
    update_download_status method of the provided tool card, and starts the
    timer with a 10-second interval. It also triggers an immediate update of
    th image download status.
    """
    timer = QTimer()
    timer.timeout.connect(lambda: tool_card.update_download_status())
    timer.start(60000)
    tool_card.update_download_status()
    return timer
