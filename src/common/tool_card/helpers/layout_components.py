from PySide6.QtWidgets import QVBoxLayout, QHBoxLayout, QLabel, QTextEdit, QPushButton
from PySide6.QtGui import QPixmap
from PySide6.QtCore import Qt
from PySide6.QtSvgWidgets import QSvgWidget
from src.path_resolver import PathResolver


def build_layout(self):
    """
    Build the main layout for displaying tool details.
    """
    details_layout = QHBoxLayout()

    image_layout = image_section(self)
    details_layout.addLayout(image_layout)

    text_info_layout = QVBoxLayout()
    text_layout = text_section(self)
    info_layout = info_section(self)
    text_info_layout.addLayout(info_layout)
    text_info_layout.addLayout(text_layout)

    details_layout.addLayout(text_info_layout)
    return details_layout


def image_section(self):
    """
    Displays the tool's image, either as an SVG or a standard image format.
    """
    image_layout = QVBoxLayout()
    image_label = QLabel(self)
    image_path = PathResolver.resource_path(self.tool["image"])

    if self.tool["image"].endswith(".svg"):
        svg_widget = QSvgWidget(image_path)
        svg_widget.setFixedSize(150, 150)
        image_layout.addWidget(svg_widget)
    else:
        pixmap = QPixmap(image_path)
        pixmap = pixmap.scaled(150, 150, Qt.AspectRatioMode.KeepAspectRatio,
                               Qt.TransformationMode.SmoothTransformation)
        image_label.setPixmap(pixmap)
        image_layout.addWidget(image_label)

    return image_layout


def info_section(self):
    """
    Displays the tool's website, size, and version information.
    """
    info_layout = QVBoxLayout()

    website_label = QLabel(f'Website: <a href="{self.tool["website"]}">{self.tool["website"]}</a>', self)
    website_label.setTextFormat(Qt.TextFormat.RichText)
    website_label.setTextInteractionFlags(Qt.TextInteractionFlag.TextBrowserInteraction)
    website_label.setOpenExternalLinks(True)
    website_label.setWordWrap(True)
    info_layout.addWidget(website_label)

    size_label = QLabel(f"Size: {self.tool['size']}", self)
    info_layout.addWidget(size_label)

    version_label = QLabel(f"Version: {self.tool['version']}", self)
    info_layout.addWidget(version_label)

    return info_layout


def text_section(self):
    """
    Displays the tool's description in a read-only text edit widget.
    """
    text_layout = QVBoxLayout()

    description_text = QTextEdit(self.tool["description"], self)
    description_text.setReadOnly(True)
    text_layout.addWidget(description_text)

    return text_layout


def usage_examples_section(self, layout):
    """
    Displays CLI usage examples for the tool in a styled text edit widget.
    """
    examples_label = QLabel("CLI usage examples", self)
    layout.addWidget(examples_label)
    examples_text = QTextEdit(self)
    examples_text.setReadOnly(True)
    examples_text.setStyleSheet("""
         background-color: #000000;
         color: #00FF00;
         font-family: 'Courier New', Courier, monospace;
         border-radius: 10px;
     """)
    examples_text.setPlainText("\n\n".join(self.tool["examples"]))
    layout.addWidget(examples_text)


def container_buttons_section(self, layout):
    """
    Create and add the layout control buttons section
    ('Run Container' and 'Stop Container') to the provided layout.
    """
    buttons_layout = QHBoxLayout()
    self.run_button = QPushButton('Run Container', self)
    buttons_layout.addWidget(self.run_button)
    self.stop_button = QPushButton('Stop Container', self)
    buttons_layout.addWidget(self.stop_button)
    layout.addLayout(buttons_layout)
