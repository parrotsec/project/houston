from PySide6.QtWidgets import QVBoxLayout, QHBoxLayout, QLabel
from PySide6.QtGui import QPixmap
from PySide6.QtCore import Qt
from PySide6.QtSvgWidgets import QSvgWidget
from src.path_resolver import PathResolver


class DetailsSection:
    def __init__(self, parent):
        self.parent = parent
        self.tool = parent.tool

    def create(self):
        details_layout = QHBoxLayout()

        image_layout = self._create_image_section()
        details_layout.addLayout(image_layout)

        info_layout = QVBoxLayout()
        info_layout.addLayout(self._create_metadata_section())
        info_layout.addLayout(self._create_description_section())

        details_layout.addLayout(info_layout)
        return details_layout

    def _create_image_section(self):
        image_layout = QVBoxLayout()
        image_path = PathResolver.resource_path(self.tool["image"])

        if self.tool["image"].endswith(".svg"):
            widget = self._create_svg_widget(image_path)
        else:
            widget = self._create_image_label(image_path)

        image_layout.addWidget(widget)
        return image_layout

    def _create_svg_widget(self, path):
        svg_widget = QSvgWidget(path)
        svg_widget.setFixedSize(150, 150)
        return svg_widget

    def _create_image_label(self, path):
        image_label = QLabel(self.parent)
        pixmap = QPixmap(path).scaled(
            150, 150,
            Qt.AspectRatioMode.KeepAspectRatio,
            Qt.TransformationMode.SmoothTransformation
        )
        image_label.setPixmap(pixmap)
        return image_label

    def _create_metadata_section(self):
        layout = QVBoxLayout()

        website_label = QLabel(
            f'Website: <a href="{self.tool["website"]}">{self.tool["website"]}</a>',
            self.parent
        )
        website_label.setTextFormat(Qt.TextFormat.RichText)
        website_label.setTextInteractionFlags(Qt.TextInteractionFlag.TextBrowserInteraction)
        website_label.setOpenExternalLinks(True)
        website_label.setWordWrap(True)
        layout.addWidget(website_label)

        layout.addWidget(QLabel(f"Size: {self.tool['size']}", self.parent))
        layout.addWidget(QLabel(f"Version: {self.tool['version']}", self.parent))

        return layout

    def _create_description_section(self):
        from PySide6.QtWidgets import QTextEdit
        layout = QVBoxLayout()
        description = QTextEdit(self.tool["description"], self.parent)
        description.setReadOnly(True)
        layout.addWidget(description)
        return layout