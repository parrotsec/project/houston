from PySide6.QtWidgets import QVBoxLayout, QLineEdit


def _get_placeholder_text(tool_name):
    placeholders = {
        "nmap": "Enter flags (e.g., -p 80 192.168.0.1)",
        "bettercap": "Enter flags (e.g., --proxy)",
        "sqlmap": "Enter flags (e.g., -u 'http://example.com')",
        "netexec": "Enter flags (e.g., --help)",
        "evil-winrm": "Enter flags (e.g., --help)"
    }
    return placeholders.get(tool_name, "Enter flags for the tool")


class FlagsSection:
    SUPPORTED_TOOLS = ["nmap", "bettercap", "sqlmap", "netexec", "evil-winrm"]

    def __init__(self, parent):
        self.parent = parent
        self.tool = parent.tool

    def create(self):
        layout = QVBoxLayout()

        tool_name = self.tool["name"].lower()
        if tool_name in self.SUPPORTED_TOOLS:
            flags_input = QLineEdit(self.parent)
            flags_input.setPlaceholderText(_get_placeholder_text(tool_name))
            layout.addWidget(flags_input)
            self.parent.flags_input = flags_input

        return layout

