from PySide6.QtWidgets import QLabel, QTextEdit, QVBoxLayout


class ExamplesSection:
    def __init__(self, parent):
        self.parent = parent
        self.tool = parent.tool

    def create(self):
        layout = QVBoxLayout()

        examples_label = QLabel("CLI usage examples", self.parent)
        layout.addWidget(examples_label)

        examples_text = QTextEdit(self.parent)
        examples_text.setReadOnly(True)
        examples_text.setStyleSheet("""
            background-color: #000000;
            color: #00FF00;
            font-family: 'Courier New', Courier, monospace;
            border-radius: 10px;
        """)
        examples_text.setPlainText("\n\n".join(self.tool["examples"]))
        layout.addWidget(examples_text)

        return layout