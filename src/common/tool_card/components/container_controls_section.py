from PySide6.QtWidgets import QHBoxLayout, QPushButton


class ControlSection:
    def __init__(self, parent):
        self.parent = parent

    def create(self):
        layout = QHBoxLayout()

        self.parent.run_button = QPushButton('Run Container', self.parent)
        layout.addWidget(self.parent.run_button)

        self.parent.stop_button = QPushButton('Stop Container', self.parent)
        layout.addWidget(self.parent.stop_button)

        return layout