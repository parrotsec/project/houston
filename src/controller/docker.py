import subprocess

from PySide6.QtGui import QIcon
from PySide6.QtWidgets import (
    QProgressDialog, QPushButton, QVBoxLayout, QDialog, QLabel, QComboBox, QHBoxLayout
)
from PySide6.QtCore import Qt

from src.path_resolver import PathResolver


class DockerController:
    """
    Provides static methods to manage Docker containers and images.
    """
    @staticmethod
    def check_container_status(tool, tool_item, download_status_label, run_button):
        """
        Check if Docker is running and if the Docker image is downloaded,
        then update the status label and run button.
        """
        docker_image = tool["docker_image"]

        try:
            subprocess.run(['docker', 'info'],
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE,
                           check=True)
        except subprocess.CalledProcessError:
            return

        try:
            result = subprocess.run(['docker', 'images', '-q', docker_image],
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE,
                                    check=True)
            if result.stdout.strip():
                tool_item.setIcon(0, QIcon(PathResolver.resource_path('resources/assets/check.png')))
            else:
                tool_item.setIcon(0, QIcon(PathResolver.resource_path('resources/assets/cross.png')))
        except subprocess.CalledProcessError as e:
            download_status_label.setText(
                f"Image download verification is not possible\n {e}")
            tool_item.setIcon(0, QIcon(PathResolver.resource_path('resources/assets/cross.png')))

        try:
            result = subprocess.run(['docker', 'ps', '-q', '-f', f'ancestor={docker_image}'],
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE,
                                    check=True)
            container_ids = result.stdout.strip().decode('utf-8').split('\n')
            if container_ids and container_ids[0]:
                if len(container_ids) > 1:
                    run_button.setText("Containers Running")
                else:
                    run_button.setText("Container Running")
            else:
                run_button.setText("Run Container")
        except subprocess.CalledProcessError as e:
            run_button.setText(
                f"Container status check is not possible\n {e}")

    @staticmethod
    def stop_container(tool, parent_widget):
        """
        Stop the Docker container associated with the specified tool by displaying a progress dialog.
        """
        docker_image = tool["docker_image"]
        progress_dialog = QProgressDialog("Stopping container...", "Cancel", 0, 100, parent_widget)
        progress_dialog.setWindowModality(Qt.WindowModality.WindowModal)
        progress_dialog.setAutoClose(False)
        progress_dialog.setAutoReset(False)
        progress_dialog.show()

        try:
            result = subprocess.run(['docker', 'ps', '-q', '-f', f'ancestor={docker_image}'],
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE,
                                    check=True)
            container_ids = result.stdout.strip().decode('utf-8').split('\n')
            progress_dialog.setValue(30)

            if container_ids and container_ids[0]:
                container_id = None
                if len(container_ids) > 1:
                    dialog = QDialog(parent_widget)
                    dialog.setWindowTitle("Select Container")

                    layout = QVBoxLayout(dialog)

                    label = QLabel("Choose container ID to stop:")
                    layout.addWidget(label)

                    combo_box = QComboBox()
                    combo_box.addItems([f"{cid}" for cid in container_ids])
                    layout.addWidget(combo_box)

                    button_layout = QHBoxLayout()

                    stop_all_button = QPushButton("Stop All Containers")
                    ok_button = QPushButton("Ok")
                    cancel_button = QPushButton("Cancel")

                    button_layout.addWidget(stop_all_button)
                    button_layout.addWidget(cancel_button)
                    button_layout.addWidget(ok_button)

                    layout.addLayout(button_layout)

                    def handle_stop_all():
                        dialog.accept()
                        DockerController.stop_all_containers(tool, parent_widget)

                    stop_all_button.clicked.connect(handle_stop_all)
                    ok_button.clicked.connect(dialog.accept)
                    cancel_button.clicked.connect(dialog.reject)

                    dialog.setLayout(layout)
                    dialog.exec()

                    if dialog.result() == QDialog.DialogCode.Accepted:
                        item = combo_box.currentText()
                        parts = item.split(': ')
                        if len(parts) > 0:
                            container_id = parts[0]
                        else:
                            print(f"Unexpected format for selected item: {item}")
                else:
                    container_id = container_ids[0]

                if container_id:
                    stop_process = subprocess.run(['docker', 'stop', container_id],
                                                  stdout=subprocess.PIPE,
                                                  stderr=subprocess.PIPE,
                                                  check=True)
                    progress_dialog.setValue(50)
                    if stop_process.returncode == 0:
                        print(f"Container {container_id} stopped successfully.")
                    else:
                        print(f"Failed to stop container {container_id}.")
                    progress_dialog.setValue(100)
                else:
                    print("No container selected to stop.")
            else:
                print("No running container found for this tool.")
        except subprocess.CalledProcessError as e:
            print(f"Failed to stop container: {e}")
        finally:
            progress_dialog.close()

    @staticmethod
    def stop_all_containers(tool, parent_widget):
        """
        Stop all Docker containers associated with the specified tool by displaying a progress dialog.
        """
        docker_image = tool["docker_image"]
        progress_dialog = QProgressDialog("Stopping all containers...", "Cancel", 0, 100, parent_widget)
        progress_dialog.setWindowModality(Qt.WindowModality.WindowModal)
        progress_dialog.setAutoClose(False)
        progress_dialog.setAutoReset(False)
        progress_dialog.show()

        try:
            result = subprocess.run(['docker', 'ps', '-q', '-f', f'ancestor={docker_image}'],
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE,
                                    check=True)
            container_ids = result.stdout.strip().decode('utf-8').split('\n')
            progress_dialog.setValue(30)

            if container_ids and container_ids[0]:
                for i, container_id in enumerate(container_ids):
                    stop_process = subprocess.run(['docker', 'stop', container_id],
                                                  stdout=subprocess.PIPE,
                                                  stderr=subprocess.PIPE,
                                                  check=True)
                    progress_dialog.setValue(30 + int(70 * (i + 1) / len(container_ids)))
                    if stop_process.returncode == 0:
                        print(f"Container {container_id} stopped successfully.")
                    else:
                        print(f"Failed to stop container {container_id}.")
                progress_dialog.setValue(100)
            else:
                print("No running container found for this tool.")
        except subprocess.CalledProcessError as e:
            print(f"Failed to stop containers: {e}")
        finally:
            progress_dialog.close()
