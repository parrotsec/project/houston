"""
This module provides functions to set up an information layout and display an
information dialog.
"""

from PySide6.QtWidgets import QVBoxLayout, QHBoxLayout, QLabel, QDialog, QPushButton
from PySide6.QtGui import QIcon, QPixmap
from PySide6.QtCore import QSize, Qt
from src.path_resolver import PathResolver
from src.theme import change_theme, show_theme_selector, get_current_theme


def setup_info_layout(main_widget, main_layout):
    """
    Set up the info layout for the main widget.

    Adds an 'Info' button with an icon to the layout, sets its tooltip, and connects it to
    the 'show_info' function. Also, adds the Docker status label and icon to the layout.
    """
    info_layout = QHBoxLayout()

    info_button = QPushButton('Info')
    info_button.setFixedSize(80, 30)
    info_button.setToolTip('Info')
    info_icon = PathResolver.resource_path('resources/assets/info.png')
    info_button.setIcon(QIcon(info_icon))
    info_button.setIconSize(QSize(14, 14))
    info_button.clicked.connect(show_info)

    info_layout.addWidget(info_button)
    info_layout.addStretch()

    info_layout.addWidget(main_widget.docker_status_label)
    info_layout.addWidget(main_widget.docker_status_icon)

    main_layout.addLayout(info_layout)


def show_info():
    """
    Display an information dialog with details about the application.

    The dialog includes the application logo, version information, usage instructions,
    and links to the source code. It also provides buttons to change the theme and close the dialog.
    """
    info_dialog = QDialog()
    info_dialog.setWindowTitle('Info')
    info_dialog.setFixedSize(400, 500)
    layout = QVBoxLayout()

    logo_label = QLabel()
    logo_path = PathResolver.resource_path('resources/assets/rocket.svg')
    logo_pixmap = QPixmap(logo_path)
    if not logo_pixmap.isNull():
        logo_pixmap = logo_pixmap.scaled(
            100,
            100,
            Qt.AspectRatioMode.KeepAspectRatio,
            Qt.TransformationMode.SmoothTransformation
        )
        logo_label.setPixmap(logo_pixmap)
        logo_label.setFixedSize(100, 100)
        logo_layout = QHBoxLayout()
        logo_layout.addStretch()
        logo_layout.addWidget(logo_label)
        logo_layout.addStretch()
        layout.addLayout(logo_layout)
    else:
        print("Rocket logo file was not found.")

    check_icon = PathResolver.resource_path('resources/assets/check.png')
    cross_icon = PathResolver.resource_path('resources/assets/cross.png')

    info_text = QLabel()
    info_text.setWordWrap(True)
    info_text.setText(
        '<div style="text-align: center;">'
        'Rocket v1.2.2<br><br>'
        'This application simplifies running security tools in Docker containers. '
        'Select a tool from the list, optionally add flags, and click "Run Container" '
        'to start the tool in a new terminal window.<br>'
        '<div>'
        f'<img src="{check_icon}" width="14" height="14"/> Image downloaded<br>'
        f'<img src={cross_icon} width="14" height="14"/> Image not downloaded<br><br>'
        '</div>'
        '<a href="https://gitlab.com/parrotsec/project/rocket">GitLab Source Code</a'
        '><br><br>'
        'Developed and maintained by <a href="mailto:danterolle@parrotsec.org"> '
        'Dario Camonita</a><br><br>'
        'This program comes with absolutely no warranty.<br>'
        'See <a href="https://www.gnu.org/licenses/gpl-3.0-standalone.html">GPLv3</a> '
        'for details.<br><br>'
        '<a href="https://www.parrotsec.org/team">ParrotSec Team</a> - 2025'
        '</div>')
    info_text.setOpenExternalLinks(True)
    layout.addWidget(info_text)

    button_layout = QHBoxLayout()
    theme_button = QPushButton('Change Theme')
    theme_button.clicked.connect(show_theme_selector)
    button_layout.addWidget(theme_button)

    close_button = QPushButton('Close')
    close_button.clicked.connect(info_dialog.accept)
    button_layout.addWidget(close_button)
    layout.addLayout(button_layout)

    current_theme = get_current_theme()
    change_theme(current_theme)

    info_dialog.setLayout(layout)
    info_dialog.exec()
