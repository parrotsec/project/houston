"""
- Loads tool information from 'tools.json' upon initialization.
- Constructs the main user interface with a tree view (`QTreeWidget`)
  for tool categories and a stacked widget (`QStackedWidget`) to display tool
  details using `ToolCard` instances.
- Allows selection of tools and displays their details dynamically.
- Provides functionality to run selected tools in Docker containers with user-defined flags.
- Implements themes for the interface and displays a credits dialog
  with information about the application and its developer.
"""

import json
import re
import subprocess

from PySide6.QtWidgets import (
    QWidget, QVBoxLayout, QHBoxLayout, QLabel, QTreeWidget, QStackedWidget
)
from PySide6.QtCore import QTimer
from src.path_resolver import PathResolver
from src.interface.dialog_info import setup_info_layout
from src.interface.tool_setup import display_tool_list
from src.interface.container_status import init_docker_status_update


class App(QWidget):
    """
    Main window for the application.
    """
    def __init__(self):
        """
        Set up the user interface for the main window.
        """
        super().__init__()
        self.tool_list = QTreeWidget()
        self.docker_status_label = QLabel('', self)
        self.docker_status_icon = QLabel(self)
        self.timer = QTimer()
        self.tool_display = QStackedWidget()
        self.tool_cards = []
        self.tools = self.load_tools()

        self.setWindowTitle('Rocket')
        self.setGeometry(100, 100, 600, 500)
        main_layout = QVBoxLayout()
        content_layout = QHBoxLayout()

        self.tool_list.setHeaderHidden(True)
        display_tool_list(self)
        content_layout.addWidget(self.tool_list)
        content_layout.addWidget(self.tool_display)

        main_layout.addLayout(content_layout)

        setup_info_layout(self, main_layout)

        init_docker_status_update(self)

        self.setLayout(main_layout)

    @staticmethod
    def load_tools():
        """
        Load tool information from 'tools.json'.
        """
        tools_path = PathResolver.resource_path('resources/tools.json')
        # work only if path absolute
        with open(tools_path, 'r', encoding='utf-8') as file:
            return json.load(file)

    @staticmethod
    def validate_flags(flags: str, tool_name: str) -> bool:
        """
        Validate the flags entered by the user.
        """
        ALLOWED_CHARS = set("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_. ")
        MAX_LENGTH = 80

        if len(flags) > MAX_LENGTH:
            return False

        if not all(c in ALLOWED_CHARS for c in flags):
            return False

        BLACKLIST_PATTERNS = [r';', r'&&', r'\|\|', r'`', r'\$\(.*\)', r'<', r'>']

        for pattern in BLACKLIST_PATTERNS:
            if re.search(pattern, flags):
                return False

        if tool_name == "nmap":
            NMAP_ALLOWED_FLAGS = [
                '--append-output', '--badsum', '--data', '--data-length', '--data-string', '--datadir',
                '--dns-servers', '--exclude', '--exclude-ports', '--excludefile', '--host-timeout',
                '--iflist', '--ip-options', '--max-rate', '--max-retries', '--max-scan-delay',
                '--min-hostgroup', '--min-parallelism', '--min-rate', '--min-rtt-timeout', '--mtu',
                '--no-stylesheet', '--noninteractive', '--open', '--osscan-guess', '--osscan-limit',
                '--packet-trace', '--port-ratio', '--privileged', '--proxies', '--reason', '--resume',
                '--scan-delay', '--scanflags', '--script', '--script-args', '--script-args-file',
                '--script-help', '--script-trace', '--script-updatedb', '--send-eth', '--send-ip',
                '--source-port', '--spoof-mac', '--stylesheet', '--system-dns', '--top-ports',
                '--traceroute', '--ttl', '--unprivileged', '--version-all', '--version-intensity',
                '--version-light', '--version-trace', '--webxml', '-2', '-5', '-6', '-A', '-D', '-E', '-F',
                '-O', '-P', '-R', '-S', '-T', '-V', '-b', '-c', '-d', '-e', '-f', '-g', '-h', '-i', '-l',
                '-n', '-o', '-p', '-r', '-s', '-t', '-v', '-sT', '-sU', '-sN', '-sF', '-sX', '-sC', '-sV',
                '-Pn', '-T0', '-T1', '-T2', '-T3', '-T4', '-T5', '-oN', '-oX', '-oG', '-oA', '-iL', '-iR'
            ]
            flags_list = re.findall(r'(?:[^\s,"]|"(?:\\.|[^"])*")+', flags)
            for flag in flags_list:
                if flag.startswith('-'):
                    flag_base = flag.split('=')[0]
                    if flag_base not in NMAP_ALLOWED_FLAGS:
                        return False
        elif tool_name == "bettercap":
            BETTERCAP_ALLOWED_FLAGS = [
                '--help', '-version', '--env', '-script', '-eval', '-silent', '-no-colors',
                '-autostart', '-caplet', '-caplets-path', '-cpu-profile', '-debug', '-env-file',
                '-gateway-override', '-iface', '-mem-profile', '-no-history', '-pcap-buf-size',
                '--sniffer-source', '--sniffer-destination', '--httpd', '--httpd-port', '--dns',
                '--dns-port', '--dns-ttl', '--dns-fake-ip', '--dns-fake', '--dns-server', '--dns-domain',
                '--dns-resolve', '--dns-spoof', '--dns-gateway', '--dns-dnscrypt', '--dns-dnscrypt-port',
                '--dns-dnscrypt-proxy', '--dns-dnscrypt-resolver', '--dns-dnscrypt-local', '--dns-dnscrypt-ec',
                '--dns-dnscrypt-version', '--dns-dnscrypt-curve', '--dns-dnscrypt-provider', '--dns-dnscrypt-key',
                '--dns-dnscrypt-dnssec', '--dns-dnscrypt-dnssec-valid', '--dns-dnscrypt-dnssec-non-valid'
            ]
            flags_list = flags.split()
            for flag in flags_list:
                if flag.startswith('-') and flag not in BETTERCAP_ALLOWED_FLAGS:
                    return False
        elif tool_name == "sqlmap":
            SQLMAP_ALLOWED_FLAGS = [
                '-u', '--url', '--dbs', '--tables', '--columns', '--dump', '--dump-all', '--search',
                '--sql-query', '--sql-shell', '--sql-file', '--batch', '--threads', '--tamper',
                '--random-agent', '--proxy', '--tor', '--check-tor', '--tor-type', '--level', '--risk',
                '--string', '--not-string', '--regexp', '--code', '--text-only', '--titles', '--technique',
                '--time-sec', '--union-cols', '--union-char', '--union-from', '--dns-domain', '--second-order',
                '--delay', '--timeout', '--retries', '--randomize', '--scope', '--alert', '--safe-url',
                '--safe-freq', '--mobile', '--os', '--os-shell', '--os-pwn', '--os-smbrelay', '--os-bof',
                '--priv-esc', '--msf-path', '--tmp-path', '--wizard', '--answers', '--safe-url', '--auth-type',
                '--auth-cred', '--auth-cert', '--auth-cert-key', '--auth-verify', '--auth-pwd', '--auth-sso',
                '--for-mysql', '--for-mssql', '--for-postgres', '--for-oracle', '--for-sqlite', '--for-access',
                '--for-firebird', '--for-sybase', '--for-maxdb', '--for-db2', '--for-informix', '--batch',
                '--flush-session', '--forms', '--crawl', '--crawl-exclude', '--crawl-depth', '--crawl-spider',
                '--forms', '--csrf-token', '--csrf-url', '--csrf-value', '--csrf-cookie', '--force-ssl',
                '--force-ssl-verify', '--force-ssl-no-verify', '--force-ssl-ciphers', '--force-ssl-protocols',
                '--force-ssl-versions', '--force-ssl-cert', '--force-ssl-key', '--force-ssl-pwd', '--force-ssl-sni',
                '--force-ssl-dns', '--force-ssl-ca', '--force-ssl-ca-path', '--force-ssl-ignore-cts',
                '--force-ssl-no-cts', '--force-ssl-no-staple', '--force-ssl-no-hsts', '--force-ssl-ignore-timestamp',
                '--force-ssl-ignore-expiration', '--force-ssl-ignore-revocation', '--force-ssl-no-revocation',
                '--force-ssl-no-tls1', '--force-ssl-no-tls1_1', '--force-ssl-no-tls1_2', '--force-ssl-no-tls1_3'
            ]
            flags_list = flags.split()
            for flag in flags_list:
                if flag.startswith('--') and flag not in SQLMAP_ALLOWED_FLAGS:
                    return False
        elif tool_name == "evil-winrm":
            EVILWINRM_ALLOWED_FLAGS = [
               '-S', '--ssl', '-c', '--pub-key', '-k', '--priv-key', '-r', '--realm', '-s',
               '--scripts', '--spn', '-e', '--executables', '-i', '--ip', '-U', '--url',
               '-u', '--user', '-p', '--password', '-H', '--hash', '-P', '--port', '-V',
               '--version', '-n', '--no-colors', '-N', '--no-rpath-completion', '-l', '--log',
               '-h', '--help'
            ]
            flags_list = flags.split()
            for flag in flags_list:
                if flag.startswith('-') and flag not in EVILWINRM_ALLOWED_FLAGS:
                    return False

        return True

    @staticmethod
    def find_terminal_emulator() -> str | None:
        """
        Find a suitable terminal emulator executable.
        """
        TERMINAL_EMULATORS = [
            'gnome-terminal',
            'x-terminal-emulator',
            'konsole',
            'xfce4-terminal',
            'lxterminal',
            'mate-terminal'
        ]

        for emulator in TERMINAL_EMULATORS:
            if subprocess.call(['which',
                                emulator],
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE) == 0:
                return emulator
        return None
