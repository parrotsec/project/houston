"""
This module manages the setup and display of the Docker-based tools list.
"""

from PySide6.QtWidgets import QTreeWidgetItem
from PySide6.QtGui import QIcon
from src.common.tool_card.tool_widgets import ToolCard
from src.path_resolver import PathResolver
from src.interface.container_status import run_container
from src.controller.docker import DockerController
from src.common.tool_card.helpers.timer_setup import setup_timer


def display_tool_list(self):
    """
    Set up the tool list tree view and the stacked widget for displaying tool details.
    """
    self.tool_items = {}

    docker_tools_item = QTreeWidgetItem(["Docker Tools"])
    docker_tools_image = PathResolver.resource_path('resources/assets/docker.png')
    docker_tools_item.setIcon(0, QIcon(docker_tools_image))

    self.tool_list.addTopLevelItem(docker_tools_item)
    for tool in self.tools:
        tool_item = QTreeWidgetItem([tool["name"]])
        docker_tools_item.addChild(tool_item)
        self.tool_items[tool["name"]] = tool_item

        card = ToolCard(tool, tool_item)

        card.run_button.clicked.connect(lambda: run_container(self))
        card.stop_button.clicked.connect(card.stop_container)

        DockerController.check_container_status(tool, tool_item, card.download_status_label, card.run_button)

        card.tool_item = tool_item
        self.tool_cards.append(card)
        self.tool_display.addWidget(card)

        setup_timer(card)

    self.tool_list.setFixedWidth(170)
    self.tool_list.currentItemChanged.connect(
        lambda current: display_tool(self, current)
    )
    self.tool_list.expandAll()


def display_tool(self, current):
    """
    Display the selected tool in the tool display area.
    """
    if current is not None and current.parent() is not None:
        index = current.parent().indexOfChild(current)
        self.tool_display.setCurrentIndex(index)
