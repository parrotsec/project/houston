"""
Manages Docker status checking and container execution for selected tools.
"""

import subprocess
import sys
from PySide6.QtWidgets import QMessageBox
from PySide6.QtGui import QIcon
from PySide6.QtCore import QSize
from src.path_resolver import PathResolver


def init_docker_status_update(app):
    """
    Initialize the Docker status update timer.
    """
    app.timer.timeout.connect(lambda: check_docker_status(app))
    app.timer.start(5000)
    check_docker_status(app)


def check_docker_status(app):
    """
    Check if Docker is running and update the Docker status label.
    """
    running_icon = QIcon(PathResolver.resource_path('resources/assets/docker_working.png'))
    not_running_icon = QIcon(PathResolver.resource_path('resources/assets/docker_not_working.png'))

    icon_size = QSize(24, 24)
    try:
        subprocess.run(['docker', 'info'],
                       stdout=subprocess.PIPE,
                       stderr=subprocess.PIPE,
                       check=True)
        app.docker_status_label.setText("Docker is running")
        app.docker_status_icon.setPixmap(running_icon.pixmap(icon_size))
    except subprocess.CalledProcessError:
        app.docker_status_label.setText("Docker is not running")
        app.docker_status_icon.setPixmap(not_running_icon.pixmap(icon_size))


def run_container(app):
    """
    Run the Docker container for the selected tool with specified flags.
    """
    current_item = app.tool_list.currentItem()
    if current_item is not None and current_item.parent() is not None:
        parent = current_item.parent()
        current_index = parent.indexOfChild(current_item)
        current_tool = app.tools[current_index]
        current_card = app.tool_cards[current_index]

        docker_image = current_tool["docker_image"]
        flags = current_card.get_flags()
        tool_name = current_tool["name"].lower()

        if not app.validate_flags(flags, tool_name):
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Icon.Critical)
            msg.setText("Invalid flags")
            msg.setInformativeText("The flags entered are not valid for the selected tool.")
            msg.setWindowTitle("Error")
            msg.exec()
            return
        command = f'docker run --rm -it {docker_image} {flags}'
        if sys.platform.startswith('win'):
            subprocess.Popen(['start', 'cmd', '/k', command], shell=True)
        elif sys.platform.startswith('darwin'):
            script = f'''
                tell application "Terminal"
                    do script "{command}"
                end tell
            '''
            subprocess.Popen(['osascript', '-e', script])
        elif sys.platform.startswith('linux'):
            terminal_emulator = app.find_terminal_emulator()
            if terminal_emulator:
                subprocess.Popen(
                    [terminal_emulator, '-e', f'bash -c "{command}; exec $SHELL"'])
            else:
                print("No terminal emulator found.")
    else:
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Icon.Warning)
        msg.setText("Please select a valid tool.")
        msg.setInformativeText(
            "You need to select a tool from the list to run the Docker container.")
        msg.setWindowTitle("Warning")
        msg.setStandardButtons(QMessageBox.StandardButton.Ok)
        msg.exec()
