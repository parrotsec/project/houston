from PySide6.QtWidgets import QApplication, QDialog, QVBoxLayout, QComboBox, QPushButton
from PySide6.QtCore import QSettings
from src.resources.themes.styles import themes
# Consts Colors Generic
from src.resources.themes.color_consts import WHITE, BLACK
# Consts Colors Default Theme
from src.resources.themes.color_consts import BG_ITEM_DEFAULT, BORDER_ITEM_DEFAULT, BG_ITEM_SELECTED_DEFAULT
# Consts Colors Hack The Box Theme 
from src.resources.themes.color_consts import BG_ITEM_HACKTHEBOX, BORDER_BUTTON_HACKTHEBOX, BG_ITEM_SELECTED_HACKTHEBOX
# Consts Colors Greenlight Theme 
from src.resources.themes.color_consts import BG_ITEM_GREENLIGHT, BORDER_ITEM_GREENLIGHT, BG_ITEM_SELECTED_GREENLIGHT
# Consts Colors Sunset Theme 
from src.resources.themes.color_consts import BG_ITEM_SUNSET, BORDER_ITEM_SUNSET, BG_ITEM_SELECTED_SUNSET

settings = QSettings('Rocket', 'ThemeSettings')


def theme(app, theme_name):
    app.setStyleSheet(themes.get(theme_name, themes['Default']))


def get_current_theme():
    return settings.value('current_theme', 'Default')


def set_current_theme(theme_name):
    settings.setValue('current_theme', theme_name)


def change_theme(theme_name):
    app = QApplication.instance()
    theme(app, theme_name)
    set_current_theme(theme_name)
    apply_combo_box_style(theme_name)
    app.processEvents()


def apply_combo_box_style(theme_name):
    theme_styles = {
        'Default': {
            'background_color': BG_ITEM_DEFAULT,
            'text_color': WHITE,
            'selection_background_color': BG_ITEM_SELECTED_DEFAULT,
            'selection_text_color': BLACK,
            'border_color': BORDER_ITEM_DEFAULT
        },
        'Hack The Box': {
            'background_color': BG_ITEM_HACKTHEBOX,
            'text_color': WHITE,
            'selection_background_color': BG_ITEM_SELECTED_HACKTHEBOX,
            'selection_text_color': WHITE,
            'border_color': BORDER_BUTTON_HACKTHEBOX
        },
        'Greenlight': {
            'background_color': BG_ITEM_GREENLIGHT,
            'text_color': BLACK,
            'selection_background_color': BG_ITEM_SELECTED_GREENLIGHT,
            'selection_text_color': BLACK,
            'border_color': BORDER_ITEM_GREENLIGHT
        },
        'Sunset': {
            'background_color': BG_ITEM_SUNSET,
            'text_color': WHITE,
            'selection_background_color': BG_ITEM_SELECTED_SUNSET,
            'selection_text_color': BLACK,
            'border_color': BORDER_ITEM_SUNSET
        }
    }

    if theme_name not in theme_styles:
        print(f"Theme '{theme_name}' not found. Using the Default theme.")
        selected_theme = theme_styles['Default']
    else:
        selected_theme = theme_styles[theme_name]

    combo_box_style = f"""
        QComboBox {{
            padding: 5px;
            border: 1px solid {selected_theme['border_color']};
            border-radius: 10px;
            background-color: {selected_theme['background_color']};
            color: {selected_theme['text_color']};
        }}
        QComboBox::drop-down {{
            border-top-right-radius: 10px;
            border-bottom-right-radius: 10px;
            background-color: {selected_theme['background_color']};
        }}
        QComboBox QAbstractItemView {{
            selection-background-color: {selected_theme['selection_background_color']};
            selection-color: {selected_theme['selection_text_color']};
        }}
    """

    app = QApplication.instance()
    app.setStyleSheet(app.styleSheet() + combo_box_style)


def show_theme_selector():
    theme_dialog = QDialog()
    theme_dialog.setWindowTitle('Select Theme')
    theme_dialog.setFixedSize(250, 150)
    layout = QVBoxLayout()

    theme_selector = QComboBox()
    theme_selector.addItems(['Default', 'Hack The Box', 'Greenlight', 'Sunset'])
    current_theme = get_current_theme()
    if isinstance(current_theme, str):
        theme_selector.setCurrentText(current_theme)
    else:
        theme_selector.setCurrentText('Default')
    theme_selector.currentTextChanged.connect(lambda: change_theme(theme_selector.currentText()))
    layout.addWidget(theme_selector)

    apply_button = QPushButton('Apply')
    apply_button.clicked.connect(theme_dialog.accept)
    layout.addWidget(apply_button)

    apply_combo_box_style(current_theme)

    theme_dialog.setLayout(layout)
    theme_dialog.exec()
