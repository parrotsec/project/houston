# Contributing to Rocket

This is a one-man project. PRs are welcome.

## Bug Reporting

To fill a bug report, go to the Issues tab and create a new issue, and mark it as a bug report. 
If you plan to fix the bug yourself, add yourself as a contributor to the issue. 
When it is ready to merge, mention @danterolle as reviewer to review and discuss changes.

## Setting Up Your Development Environment

- A working installation of Git
- A text editor or Python IDE
- Python installed, version 3.8 or higher
- The following non-standard PyPI module (installable via pip): poetry

## Workflow

1. Fork the repository.
2. Create a new branch (git checkout -b feature/custom-feature).
3. Commit your changes (git commit -am 'add custom feature').
4. Push to the branch (git push origin feature/custom-feature).
5. Submit a new Pull Request.

Thank you!