# Rocket Project

"""
Rocket, a tool launcher for Docker-based security tools.
"""

import sys

from PySide6.QtGui import QIcon
from PySide6.QtWidgets import QApplication

from src.path_resolver import PathResolver
from src.interface.app import App
from src.theme import theme, get_current_theme

if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setWindowIcon(QIcon(PathResolver.resource_path('resources/assets/rocket.svg')))
    current_theme = get_current_theme()
    theme(app, current_theme)
    window = App()
    window.show()
    sys.exit(app.exec())
