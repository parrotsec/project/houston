.PHONY: help lint prepare build debian_package changelog debian_changelog compress clean

VERSION := $(shell poetry version -s)
SRC_DIR := src
DIST_DIR := dist
RESOURCES_DIR := $(SRC_DIR)/resources
UI_DIR := $(SRC_DIR)/interface
MAIN := main.py
DATE=$(shell date +%Y-%m-%d)

help:
	@echo "Please use 'make <target>', where <target> is one of"
	@echo ""
	@echo "  lint                runs code linting with pylint and formats code with autopep8"
	@echo "  prepare             prompts for a new version number and updates the project version"
	@echo "  build               builds the Rocket application into a standalone executable using PyInstaller"
	@echo "  debian_package      packages the application for Debian distribution and builds the Debian package"
	@echo "  changelog           creates a changelog file from the latest git commits"
	@echo "  debian_changelog    generates a Debian changelog from recent git commits and updates the changelog file"
	@echo "  compress            creates a .zip file of the built Rocket application"
	@echo "  clean               removes build artifacts and temporary files"
	@echo ""
	@echo "Check the Makefile to know exactly what each target is doing."

lint:
	@echo "Running pylint..."
	@pylint --disable=fixme --disable=no-name-in-module --disable=E0401 $(RESOURCES_DIR)/* $(UI_DIR)/*
	@echo "Running autopep8..."
	@autopep8 --in-place --aggressive $(RESOURCES_DIR)/* $(UI_DIR)/*
	@echo "[!] All done"

prepare:
	@echo "Updating project version..."
	@read -p "Enter version number: " version; \
	poetry run python $(SRC_DIR)/update_version.py --version $$version
	@echo "[!] Rocket project version updated"

changelog: debian_changelog
	@echo "Generating changelog..."
	@echo "# Changelog" > CHANGELOG.md
	@echo "" >> CHANGELOG.md
	@echo "All notable changes to this project will be documented in this file." >> CHANGELOG.md
	@echo "" >> CHANGELOG.md
	@echo "The format is based on [CHANGELOG.md](https://keepachangelog.com/en/1.1.0/) and this project adheres to [Semantic Versioning](https://semver.org/)." >> CHANGELOG.md
	@echo "_______________________________________________________________________________" >> CHANGELOG.md
	@echo "## [$(VERSION)] - $(DATE)" >> CHANGELOG.md
	@echo "" >> CHANGELOG.md
	@git log $(shell git describe --tags --abbrev=0)..HEAD --pretty=format:"- %s (%h)" >> CHANGELOG.md
	@echo "[!] Changelog generated"

build:
	@echo "Building Rocket..."
	@poetry run pyinstaller --windowed --onefile -y -n "rocket_$(VERSION)" --add-data "$(RESOURCES_DIR)/tools.json:./resources" --add-data "$(RESOURCES_DIR)/assets/*:./resources/assets" --icon=rocket.ico $(MAIN)
	@echo "[!] All done"

debian_package:
	@echo "Building Rocket for Debian..."
	@python3 convert_pyproject.py
	@cd .. && tar czf rocket_$(VERSION).orig.tar.gz rocket/ && cd rocket/
	# export PYTHON_KEYRING_BACKEND=keyring.backends.null.Keyring
	@dpkg-buildpackage -us -uc --source-option=--include-binaries
	@echo "[!] All done"

debian_changelog:
	@echo "Generating Debian changelog..."
	@new_version="rocket ($(VERSION)-1) unstable; urgency=medium"; \
	echo "$$new_version" > debian/tmpchangelog; \
	echo "" >> debian/tmpchangelog; \
	git log $(shell git describe --tags --abbrev=0)..HEAD --pretty=format:"  * %s (%h)" >> debian/tmpchangelog; \
	echo "" >> debian/tmpchangelog; \
	echo "" >> debian/tmpchangelog; \
	echo " -- Dario Camonita <danterolle@parrotsec.org>  $(shell date -R)" >> debian/tmpchangelog; \
	echo "" >> debian/tmpchangelog; \
	cat debian/changelog >> debian/tmpchangelog; \
	mv debian/tmpchangelog debian/changelog
	@echo "[!] Debian changelog generated"

compress:
	@echo "Creating .zip file... rocket_$(VERSION).zip"
	@zip -r "rocket_$(VERSION).zip" $(DIST_DIR)/rocket_$(VERSION)

clean:
	@echo "Removing build files..."
	@rm -rf build/ $(DIST_DIR)/ rocket_$(VERSION).spec .pybuild src/rocket.egg-info
	@echo "Removing Debian build temp files..."
	@rm -rf debian/rocket debian/source/include-binaries debian/.debhelper debian/debhelper-build-stamp debian/rocket.substvars debian/files debian/include-binaries
	@echo "[!] All done"