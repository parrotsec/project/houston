# Rocket v1.2.2

[![pipeline status](https://gitlab.com/parrotsec/project/rocket/badges/main/pipeline.svg)](https://gitlab.com/parrotsec/project/rocket/-/commits/main)

Rocket is a launcher for Docker-based security tools, built using PySide6. 
This application provides users with an easy interface for selecting and running various Docker tools.

This launcher has been tested on ParrotOS 6.1 and MacOS 14. It should also work on Windows 10/11.

- [Features](#features)
- [Installation](#installation)
- [Usage](#usage)
- [Troubleshooting](#troubleshooting-on-linux)
- [Contributing](#contributing)
- [Contact](#contact)
- [License](#license)

![Rocket on ParrotOS](rocket.png "Rocket on ParrotOS")

## Features

- **Tool list**: easily select a tool from the list of available tools.
- **Tool Execution**: run selected tool with user-defined flags in a new terminal window.
- **Container Status Monitoring**: automatically check and display the running status of Docker containers.

## Installation

### Pre-requirements

In order for the application to work, you must install Docker.

```bash
sudo apt update && sudo apt install docker.io
```

If you are running Linux:

```bash
sudo apt install libxcb-cursor-dev
```

### Requirements

- Python 3.11+
- PySide6
- Poetry

## Run it locally

1. Clone the repository:

```bash
git clone https://gitlab.com/parrotsec/project/rocket.git
cd rocket
```

2. Install dependencies

```bash
poetry install
poetry run python main.py
```

### Makefile Targets

Please use `make <target>`, where `<target>` is one of the following:

- **lint**  
  Runs code linting with pylint and formats code with autopep8.

- **prepare**  
  Prompts for a new version number and updates the project version.

- **build**  
  Builds the Rocket application into a standalone executable using PyInstaller.

- **debian_package**  
  Packages the application for Debian distribution and builds the Debian package.

- **changelog**  
  Creates a changelog file from the latest git commits.

- **debian_changelog**  
  Generates a Debian changelog from recent git commits and updates the changelog file.

- **compress**  
  Creates a `.zip` file of the built Rocket application.

- **clean**  
  Removes build artifacts and temporary files.

Check the `Makefile` to know exactly what each target is doing.

## Build process

To build the application:

```bash
make build
```

Pyinstaller is used, and all dependencies and assets are added to the binary. 
Once the build process is complete, the **dist** folder will be created. 
The executable **rocket_VERSION** will be inside this folder.

## Usage

- Launch the application and select a tool category from the sidebar.
- Click on a tool to view detailed information in the main panel.
- Optionally enter flags for tools like 'Nmap', 'sqlmap', etc...
- Click "Run Container" to execute the tool in a Docker container.
- You can check if the container has been started. If necessary, you can stop it.

## Troubleshooting (on Linux)

It could cause keyring errors when running any Poetry command, see https://github.com/python-poetry/poetry/issues/1917

To fix this, although it is an inconvenient and temporary solution, set: 

```bash
export PYTHON_KEYRING_BACKEND=keyring.backends.null.Keyring
```

## Contributing

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](CODE_OF_CONDUCT.md)

See CONTRIBUTING.md

## Contact

Developed and maintained by Dario Camonita from ParrotSec Team: https://www.parrotsec.org/team 

To provide feedback, report problems, or ask questions about the application contact: <danterolle@parrotsec.org>

## License

This project is licensed under the GNU General Public License v3.0. See LICENSE for more details.
