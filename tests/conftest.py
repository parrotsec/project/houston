import json
import pytest
from unittest.mock import patch, mock_open

TOOLS = """
[
  {
    "name": "Metasploit",
    "description": "Metasploit description",
    "image": "resources/assets/metasploit.svg",
    "docker_image": "metasploitframework/metasploit-framework",
    "website": "https://www.metasploit.com/",
    "version": "6.1.29",
    "size": "~1.69 GB",
    "examples": ["msfconsole -q\\nuse exploit/multi/handler"]
  },
  {
    "name": "Nmap",
    "description": "Nmap",
    "image": "resources/assets/nmap.png",
    "docker_image": "parrotsec/nmap",
    "website": "https://nmap.org/",
    "version": "7.92",
    "size": "~642 MB",
    "examples": [
       "nmap -sP 192.168.1.0/24",
       "nmap -sS -p 80,443 192.168.1.1",
       "nmap -sV 192.168.1.1"
     ]
  }
]
"""


@pytest.fixture
def tools():
    with patch("builtins.open", mock_open(read_data=TOOLS)):
        yield json.loads(TOOLS)


@pytest.fixture
def subprocess_run():
    with patch("subprocess.run") as mock_run:
        yield mock_run


@pytest.fixture
def subprocess_popen():
    with patch("subprocess.Popen") as mock_popen:
        yield mock_popen


@pytest.fixture
def subprocess_call():
    with patch("subprocess.call") as mock_call:
        mock_call.return_value = 0
        yield mock_call


@pytest.fixture
def subprocess_mock(subprocess_run, subprocess_popen, subprocess_call):
    return subprocess_run, subprocess_popen, subprocess_call
