import subprocess
from unittest.mock import MagicMock
from PySide6.QtGui import QIcon
from PySide6.QtCore import QSize
import pytest
from src.interface.app import App
from src.path_resolver import PathResolver


def assert_docker_status_icon(to_check, expected_resource):
    icon_size = QSize(24, 24)
    expected = QIcon(
        PathResolver.resource_path(expected_resource)).pixmap(icon_size)
    expected = expected.copy(expected.rect())
    pixmap = to_check.pixmap().copy(expected.rect())
    assert expected.toImage() == pixmap.toImage()


@pytest.mark.parametrize("is_working,label,expected_icon", [
    (True, "Docker is running", 'resources/assets/docker_working.png'),
    (False, "Docker is not running",
     'resources/assets/docker_not_working.png'),
])
def test_docker_status(is_working, label, expected_icon, qtbot, tools,
                       subprocess_mock):
    subprocess_run, _, _ = subprocess_mock

    if not is_working:

        def fake_subprocess_run(*args, **kwargs):
            mock_result = MagicMock()
            arguments = args[0]
            if len(arguments) > 1 and arguments[1] == 'info':
                raise subprocess.CalledProcessError(-1, "docker")
            return mock_result

        subprocess_run.side_effect = fake_subprocess_run

    widget = App()
    qtbot.addWidget(widget)
    status_icon = widget.docker_status_icon
    assert widget.docker_status_label.text().startswith(label)
    assert_docker_status_icon(status_icon, expected_icon)
